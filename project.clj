(defproject telharmonium "0.1.0"
  :description "A library for authoring difficult music"
  :url "http://example.com/FIXME"
  :license {:name "GPL"
            :url "https://www.gnu.org/licenses/gpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]])
