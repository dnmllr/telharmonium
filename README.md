# telharmonium

A clojure library for authoring difficult music

## License

Copyright © 2016 Dan Miller

Distributed under the [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)
