(ns telharmonium.score-test
  "Tests for the telharmonium.score namespace"
  (:require [clojure.test :refer :all]
            [telharmonium.score :refer :all]))


(deftest score-rendering
  (let [score-mock-1 '(:score {:title "Hello"
                              :composer "Dan Miller"})]
    (testing "Rendering Header"
      (is (= "\n\\header {\ntitle = \"Hello\"\ncomposer = \"Dan Miller\"\n}\n" (lily-score-header score-mock-1))))))

