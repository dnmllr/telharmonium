(ns telharmonium.score
  "Holds utilities for managing and rendering the score, which is the top level
  container for music in telharmonium"
  (:require [telharmonium.to-lily :refer [to-lily write-to-score]]))

(defn map-to-lily-key-values [m]
  "Converts a map to a set of lilypond declarations. TODO this may be better served in
  an external utility namespace. Also, should the brackets be moved into this method? Currently
  the brackets are expected to be handled by this functions caller"
  (map #(str (name (first %)) " = \"" (second %) "\"\n") m))

(defn lily-score-header [score]
  "Converts the score form's options to a lily pond header markup description"
  (str "\n\\header {\n" (apply str (map-to-lily-key-values (second score))) "}\n"))

(defn write-header-if-available [ctx form]
  "Takes a context and the current form and returns a new context and form
  with the options in the written as header output. Will drop the identifying information
  from the form leaving only the forms children in the graph. This allows for easy iteration
  after extracting the header."
  (if (map? (second form))
    [(write-to-score ctx (lily-score-header form)) (drop 2 form)]
    [ctx (rest form)]))

(defn open-score [ctx]
  "Writes the score opening lilypond source to the context"
  (write-to-score ctx "\n\\score {\n"))

(defn close-score [ctx]
  "Writes the score closing lilypond source to the context"
  (write-to-score ctx "\n}\n"))

(defmethod to-lily :score [ctx form]
  (let [[ctx form] (write-header-if-available ctx form)]
    (close-score (reduce to-lily (open-score ctx) form))))
