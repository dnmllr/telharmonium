(ns telharmonium.to-lily
  "Holds the definition for the to-lily multimethod and some methods for handling the to-lily context")

(def default-context
  "The default context object to pass into the to-lily method"
  {:lily-source ""})

(defn print-lily [ctx]
  "Takes a lily context object and prints the current source inside of it"
  (->> ctx
       (:lily-source)
       (println)))

(defn write-lily-to-file [ctx fileName]
  "Takes a lily context object and writes the current source inside of it to a file"
  (->> ctx
       (:lily-source)
       (spit fileName)))

(defn write-to-score [ctx new-source]
  "Given a context and a string returns a new context with the string appended to the score"
  (assoc ctx :lily-source (str (:lily-source ctx) new-source)))

(defmulti to-lily
  "The to-lily method takes a context map and collections of the form (:type ...) and returns
  a new context map with the :lily-source property hopefully containing valid lilypond source.
  The context object can be used to create contextual information which can be used by forms
  further down the graph."
  (fn [ctx form] (first form)))


(defmethod to-lily :default [_ form]
  "Throws an error if to-lily is called on a form without a to-lily method"
  (throw (Exception. (str "\nNo known lily markup for form " form "\n\n"))))
