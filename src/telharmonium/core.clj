(ns telharmonium.core
  (:gen-class))

(comment
  ; some implementation notes

  ; containers
  (:score )
  (:staff-group )
  (:staff (('voice)..))
  (:time )
  (:voice )


  ; events
  '(:event 'rest/note/chord/glissando 'event-annotations)
  '(:rest 'duration)
  '(:note 'pitch 'duration? 'octave?)
  '(:chord ('note))
  '(:glissando 'note-from 'note-to)

  ; event-annotations appear in a set
  (:dynamic 'some-dynamic)
  (:crescendo-start)
  (:cresendo-for)
  (:crescendo-end)
  (:descrendo-start)
  (:descrendo-for)
  (:descrendo-end)
  (:slur-start)
  (:slur-for)
  (:slur-end)
  )

(def piano {:long-name "Piano"
            :short-name "pno."})


; This definition represents a blank score
; The first goal is to render this
(def score '(:score {:title "An Example"
                     :composer "Dan Miller"
                     :date "September 2016"}
                    (:staff-group {:instrument piano
                                   :group-style :brace}
                                  (:staff {:clef :treble})
                                  (:staff {:clef :bass}))))

